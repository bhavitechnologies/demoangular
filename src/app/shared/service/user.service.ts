import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import{ User } from '../model/User.model';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  formData  : User;
  list      : User[];
  constructor(private http :  HttpClient) { }
    baseUrl: string = 'https://jsonplaceholder.typicode.com/posts';
    
    post(formData : User){
    return this.http.post(this.baseUrl,formData);
  }
}
