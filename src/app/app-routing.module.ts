import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { UserComponent } from './Test/user/user.component';
import { UserlistComponent } from './Test/userlist/userlist.component';
import { from } from 'rxjs';
const routes: Routes = [
  { path: 'user', component: UserComponent },
  {path : '', component : UserComponent},
  { path: 'userlist', component: UserlistComponent }
];

export const AppRoutingModule: ModuleWithProviders =
    RouterModule.forRoot(routes);
