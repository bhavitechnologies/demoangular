import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatIconModule,  MatSelectModule, 
 MatRadioModule,MatCheckboxModule, MatInputModule, MatButtonModule,
MatFormFieldModule} from '@angular/material' ;
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './Test/user/user.component';
import { UserlistComponent } from './Test/userlist/userlist.component';
import { UserService } from './shared/service/user.service';
import { HeaderComponent } from './Test/header/header.component';
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserlistComponent,
    HeaderComponent
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule ,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatFormFieldModule,
    CommonModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
