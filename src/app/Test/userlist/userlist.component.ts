import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatDialogConfig, MatDialog, MatTableDataSource } from '@angular/material';
import { Router } from "@angular/router";
@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
   dataSource;
   displayedColumns = ['userId', 'body', 'title'];
   response:any;
   name: number;
  constructor(
    private httpClient:HttpClient,private route:Router) { }
readonly rootURL = "https://jsonplaceholder.typicode.com/posts/"
  ngOnInit() {
    this.getbyid();
  }

GetAll(){
this.httpClient.get(this.rootURL).subscribe((Data:any)=>{
this.response = Data;
this.dataSource = new MatTableDataSource(this.response);
    });
}
getbyid() {
this.httpClient.get(this.rootURL +'1').subscribe((Data:any)=>{
if (Array.isArray(Data)) {
this.response = Data;
}
else{
  this.response=[Data];
}
this.dataSource = new MatTableDataSource(this.response);
    });
}



Search(){
var id=this.name;
this.httpClient.get(this.rootURL + id).subscribe((Data:any)=>{
if (Array.isArray(Data)) {
this.response = Data;
}
else{
  this.response=[Data];
}
this.dataSource = new MatTableDataSource(this.response);
});
}







}
