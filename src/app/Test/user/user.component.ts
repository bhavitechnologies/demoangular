import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  User: FormGroup;
  constructor(private formBuilder: FormBuilder,public service : UserService,private route:Router) { }

  ngOnInit() {
    this.validation();
  }


  validation(){
    this.User=this.formBuilder.group({
        userId:['',Validators.required],
        title:['',Validators.required],
        body:['',Validators.required]
    });
  }

Onsubmit(){
 if (this.User.invalid) {
            return;
        }
  var value=this.User.value
  this.service.post(value).subscribe (res => {
      alert("Data successfully inserted.");
      this.route.navigate(['userlist']);
    });
}


}
